import { createBrowserRouter } from 'react-router-dom';
import { IndexPage, MainPage } from '../views';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <MainPage />
  },
  {
    path: '/invoice/:id',
    element: <IndexPage />
  }
]);