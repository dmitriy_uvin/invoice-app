import { useEffect } from 'react';
import { invoices } from '../data';

export const useInitData = () => {
  useEffect(() => {
    const dataLC = localStorage.getItem('invoices') ?? '[]';
    const dataItems = JSON.parse(dataLC);
    if (!dataItems.length) {
      let result: any = {};
      for (let invoice of invoices) {
        result[invoice.id] = invoice;
      }
      localStorage.setItem('invoices', JSON.stringify(Object.values(result)));
      localStorage.setItem('invoices_object', JSON.stringify(result));
    }
  }, []);
}