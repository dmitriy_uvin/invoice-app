import { useState } from 'react';

export const useToggle = (defaultValue = false) => {
  const [toggle, setToggle] = useState<boolean>(defaultValue);

  const onToggle = () => {
    setToggle(!toggle);
  }

  return [toggle, onToggle] as const;
}