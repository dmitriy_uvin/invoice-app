import { useEffect, useState } from 'react';

export const useIsTablet = () => {
  const [isTablet, setIsTablet] = useState<boolean>(false);

  const resize = () => {
    setIsTablet(window.innerWidth < 1440 && window.innerWidth >= 768);
  }

  useEffect(() => {
    window.addEventListener('resize', resize);

    return () => window.removeEventListener('resize', resize);
  });

  return isTablet;
}