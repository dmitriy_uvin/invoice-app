import { useEffect, useState } from 'react';

export const useIsMobile = () => {
  const [isTrue, setIsTrue] = useState<boolean>(window.innerWidth < 768);

  const resize = () => {
    setIsTrue(window.innerWidth < 768);
  };

  useEffect(() => {
    window.addEventListener('resize', resize);

    return () => window.removeEventListener('resize', resize);
  }, []);

  return isTrue;
}