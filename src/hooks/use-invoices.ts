import { useEffect, useState } from 'react';
import { InvoiceEntity } from '../types';
import { invoices } from '../data';

export const useInvoices = () => {
  const [items, setItems] = useState<InvoiceEntity[]>([]);

  useEffect(() => {
    const dataLC = localStorage.getItem('invoices') ?? '[]';
    const dataItems = JSON.parse(dataLC);
    setItems(dataItems);
  }, []);

  return [items, setItems] as const;
}