import { useEffect } from 'react';

export const useInitTheme = () => {
  useEffect(() => {
    const themeMode = localStorage.getItem('theme-mode') ?? 'light';
    if (themeMode === 'light') {
      document.body.getAttribute('theme-mode');
    } else {
      document.body.setAttribute('theme-mode', 'dark');
    }
  }, []);
}