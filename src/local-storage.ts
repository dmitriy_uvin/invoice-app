import { InvoiceEntity, InvoiceStatus } from './types/invoice';

const getAllItemsObj = () => {
  const itemsLC = localStorage.getItem('invoices_object') ?? '{}';
  return JSON.parse(itemsLC);
};

const saveNewInvoices = (invoicesObj: any) => {
  localStorage.setItem('invoices_object', JSON.stringify(invoicesObj));
  localStorage.setItem('invoices', JSON.stringify(Object.values(invoicesObj)));
};

const saveNewInvoicesArray = (items: InvoiceEntity[]) => {
  let result: any = {};
  for (let item of items) {
    result[item.id] = item;
  }
  saveNewInvoices(result);
};

const deleteInvoice = (id: string) => {
  const invObj = getAllItemsObj();
  delete invObj[id];
  saveNewInvoices(invObj);
};

const getById = (id: string) => {
  const invObj = getAllItemsObj();
  return invObj[id];
};

const updateStatus = (id: string, newStatus: InvoiceStatus) => {
  const invoicesObj = getAllItemsObj();
  if (!Object.values(invoicesObj).length) return;
  invoicesObj[id] = {
    ...invoicesObj[id],
    status: newStatus
  };
  saveNewInvoices(invoicesObj);
};

const updateInvoice = (invoice: InvoiceEntity) => {
  const invoicesObj = getAllItemsObj();
  invoicesObj[invoice.id] = {
    ...invoice
  };
  saveNewInvoices(invoicesObj);
}

export {
  updateStatus,
  getAllItemsObj,
  getById,
  deleteInvoice,
  saveNewInvoices,
  saveNewInvoicesArray,
  updateInvoice,
};
