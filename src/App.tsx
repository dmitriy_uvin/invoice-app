import { RouterProvider } from 'react-router-dom';
import { router } from './router';
import { useInitData, useInitTheme } from './hooks';

const App = () => {
  useInitTheme();
  useInitData();
  return (
    <RouterProvider router={router} />
  );
};

export default App
