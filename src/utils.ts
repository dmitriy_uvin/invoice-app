import { InvoiceItemEntity } from './types/invoice';

export const generateId = () => {
  const letters = 'QAZWSXEDCRFVTGBYHNUJMIKOLP'.split('');
  const start = Math.ceil((letters.length - 3) * Math.random());
  const randomLetters = letters.slice(start, start + 2).join('');
  const numbers = Math.ceil(Math.random() * 10000);
  return `${randomLetters}${numbers}`;
}

export const getItemsTotal = (items: InvoiceItemEntity[]) => {
  return items.reduce(
    (sum: number, item: InvoiceItemEntity) => sum + item.price * item.quantity,
    0
  );
};

export const getAllItemsObj = () => {
  const itemsLC = localStorage.getItem('invoices_object') ?? '{}';
  return JSON.parse(itemsLC);
}

export const saveNewInvoices = (invoicesObj: any) => {
  localStorage.setItem('invoices_object', JSON.stringify(invoicesObj));
  localStorage.setItem('invoices', JSON.stringify(Object.values(invoicesObj)));
}

export const priceFormatter = (price: number) => price.toFixed(2);

export const dateFormatter = (value: string) => {
  const date = new Date(value).toDateString();
  const parts = date.split(' ');
  return `${parts[2]} ${parts[1]} ${parts[3]}`;
}