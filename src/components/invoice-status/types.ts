import { InvoiceStatus } from '../invoice/types';

export interface InvoiceStatusProps {
  status: InvoiceStatus;
}