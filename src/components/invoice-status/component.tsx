import { InvoiceStatusProps } from './types';
import './styles.scss';

export const InvoiceStatus = ({ status }: InvoiceStatusProps) => {
  const mappings = {
    'draft': {
      text: 'Draft',
    },
    'paid': {
      text: 'Paid',
    },
    'pending': {
      text: 'Pending',
    }
  };
  const statusData = mappings[status];

  return (
    <div
      className={`invoice__status invoice--${status}`}
    >
      <div
        className={`invoice__status__circle invoice--${status}`}
      ></div>
      <div
        className={`invoice__status__text invoice--${status}`}
      >{statusData.text}</div>
    </div>
  );
}