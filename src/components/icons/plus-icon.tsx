import plusIcon from '../../assets/icons/plus-circle-icon.svg';

export const PlusIcon = () => {
  return (
    <img src={plusIcon} alt="" style={{ verticalAlign: 'top' }} />
  );
}