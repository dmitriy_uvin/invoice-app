import chevron from '../../assets/icons/chevron-left-icon.svg';

export const ChevronLeft = () => {
  return (
    <img src={chevron} alt="" style={{ verticalAlign: 'top', display: 'block' }} />
  );
}