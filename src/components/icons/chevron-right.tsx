import chevron from '../../assets/icons/chevron-right-icon.svg';

export const ChevronRight = () => {
  return (
    <img src={chevron} alt="" style={{ verticalAlign: 'top', display: 'block' }} />
  );
}