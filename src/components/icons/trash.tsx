import icon from '../../assets/icons/trash-icon.svg';

export const Trash = () => {
  return (
    <img src={icon} alt="" style={{ verticalAlign: 'top', display: 'block' }} />
  );
}