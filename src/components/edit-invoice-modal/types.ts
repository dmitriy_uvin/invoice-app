import { InvoiceEntity } from '../../types';

export interface EditInvoiceModalProps {
  isVisible: boolean;
  onClose: () => void;
  onUpdate: (invoice: InvoiceEntity) => void;
  item: InvoiceEntity;
}

export interface EditInvoiceModalActionsProps {
  onClose: () => void;
  onSave: () => void;
}