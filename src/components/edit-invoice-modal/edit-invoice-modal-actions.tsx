import { Button } from '../button';
import { EditInvoiceModalActionsProps } from './types';

export const EditInvoiceModalActions = ({ onClose, onSave }: EditInvoiceModalActionsProps) => {
  return (
    <div className="new__invoice__actions">
      <div className="left">
        <Button
          coloring={'light'}
          rounding={'1.5rem'}
          textBold={'bold'}
          type='button'
          onClick={onClose}
        >Cancel</Button>
      </div>
      <div className="right">
        <Button
          coloring={'dark'}
          rounding={'1.5rem'}
          textBold={'bold'}
          type='button'
          onClick={onSave}
        >Save Changes</Button>
      </div>
    </div>
  );
}