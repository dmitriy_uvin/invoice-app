import { SlideModal } from '../slide-modal';
import { Typography } from '../typography';
import { EditInvoiceModalProps } from './types';
import './styles.scss';
import { Input } from '../input';
import { InputGroup } from '../input-group';
import { Row } from '../row';
import { Col } from '../col';
import { useEffect } from 'react';
import { EditInvoiceProps, InvoiceEntity } from '../../types/invoice';
import { Trash } from '../icons';
import { Button } from '../button';
import { useFieldArray, useForm, useWatch } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import { editInvoiceValidation } from './validation';
import { getItemsTotal, priceFormatter } from '../../utils';
import { Select } from '../select';
import { paymentTerms } from '../../data';
import { EditInvoiceModalActions } from './edit-invoice-modal-actions';

export const EditInvoiceModal = ({ isVisible, onClose, onUpdate, item }: EditInvoiceModalProps) => {
  const { register, handleSubmit, formState: { errors }, control } = useForm<EditInvoiceProps>({
    resolver: joiResolver(editInvoiceValidation),
    defaultValues: {
      paymentDue: item.paymentDue,
      paymentTerms: item.paymentTerms,
      description: item.description,
      status: item.status,
      senderAddress: item.senderAddress,
      clientAddress: item.clientAddress,
      clientEmail: item.clientEmail,
      clientName: item.clientName,
      items: item.items,
    }
  });

  useEffect(() => {
    console.log(errors);
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'items',
  });

  const itemsWatcher = useWatch({
    name: 'items',
    control,
  });

  const invoiceMapper = (data: EditInvoiceProps): InvoiceEntity => ({
    ...data,
    total: getItemsTotal(data.items),
  });

  const onUpdateSubmit = (data: EditInvoiceProps) => {
    onUpdate({
      ...invoiceMapper({
        ...item,
        ...data
      }),
    });
    onClose();
  };

  const onSaveChanges = () => {
    handleSubmit(onUpdateSubmit)();
  };

  const addItem = () => {
    append({
      quantity: 0,
      price: 0,
      name: '',
      total: 0,
    });
  };

  const onDeleteItem = (index: number) => {
    remove(index);
  }

  return (
    <SlideModal isVisible={isVisible}>
      <Typography type={'title'} text={'Edit Invoice'} style={{ fontSize: '1.5rem' }} />
      <form className='new__invoice__form'>
        <InputGroup title={'Bill From'}>
          <Row>
            <Col>
              <Input
                label={'Street Address'}
                {...register('senderAddress.street')}
                error={errors.senderAddress?.street?.message}
              />
            </Col>
          </Row>
          <Row cols={3} className={'app__row__3__2__sm'}>
            <Col><Input
              label={'City'}
              {...register('senderAddress.city')}
              error={errors.senderAddress?.city?.message}
            /></Col>
            <Col>
              <Input
                label={'Post Code'}
                {...register('senderAddress.postCode')}
                error={errors.senderAddress?.postCode?.message}
              />
            </Col>
            <Col><Input
              label={'Country'}
              {...register('senderAddress.country')}
              error={errors.senderAddress?.country?.message}
            /></Col>
          </Row>
        </InputGroup>
        <InputGroup title={'Bill To'}>
          <Row>
            <Col>
              <Input
                label={"Client's Name"}
                {...register('clientName')}
                error={errors.clientName?.message}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Input
                label={"Client's Email"}
                {...register('clientEmail')}
                error={errors.clientEmail?.message}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Input
                label={"Street Address"}
                {...register('clientAddress.street')}
                error={errors.clientAddress?.street?.message}
              />
            </Col>
          </Row>
          <Row cols={3} className={'app__row__3__2__sm'}>
            <Col><Input
              label={'City'}
              {...register('clientAddress.city')}
              error={errors.clientAddress?.city?.message}
            /></Col>
            <Col><Input
              label={'Post Code'}
              {...register('clientAddress.postCode')}
              error={errors.clientAddress?.postCode?.message}
            /></Col>
            <Col><Input
              label={'Country'}
              {...register('clientAddress.country')}
              error={errors.clientAddress?.country?.message}
            /></Col>
          </Row>
        </InputGroup>
        <InputGroup>
          <Row>
            <Col>
              <Input
                label={'Invoice Date'}
                type='date'
                {...register('paymentDue')}
                error={errors.paymentDue?.message}
              />
            </Col>
            <Col>
              <Select
                values={paymentTerms}
                label={'Payment Terms'}
                {...register('paymentTerms')}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Input
                label={"Project / Description"}
                {...register('description')}
                error={errors.description?.message}
                defaultValue={item.description}
              />
            </Col>
          </Row>
        </InputGroup>
        {fields.length > 0 &&
        <div className="new__invoice__items">
          <div className="new__invoice__items__header">
            <div className="header__item">Item Name</div>
            <div className="header__item">Qty.</div>
            <div className="header__item">Price</div>
            <div className="header__item">Tot.</div>
            <div className="header__item"></div>
          </div>
          {fields.length > 0 && fields.map((i, index) =>
            <Row className="new__invoice__items__data" key={i.id}>
              <Col>
                <Input
                  defaultValue={i.name}
                  {...register(`items.${index}.name`)}
                  error={errors.items ? errors.items[index]?.name?.message : ''}
                  displayError={false}
                />
              </Col>
              <Col>
                <Input
                  type='number'
                  style={{ paddingLeft: '.5rem', paddingRight: '.5rem' }}
                  defaultValue={`${i.quantity}`}
                  error={errors.items ? errors.items[index]?.quantity?.message : ''}
                  displayError={false}
                  {...register(`items.${index}.quantity`)}
                />
              </Col>
              <Col>
                <Input
                  type='number'
                  style={{ paddingLeft: '.5rem', paddingRight: '.5rem' }}
                  defaultValue={i.price}
                  {...register(`items.${index}.price`)}
                  error={errors.items ? errors.items[index]?.price?.message : ''}
                  displayError={false}
                />
              </Col>
              <Col>
                <Typography
                  type={'commonTextDark'}
                  text={`${priceFormatter(
                    (itemsWatcher ? itemsWatcher[index]?.price : 0) * (itemsWatcher ? itemsWatcher[index]?.quantity: 0)
                  )}`}
                  style={{ fontWeight: 'bold' }}
                />
              </Col>
              <Col>
                <div onClick={() => onDeleteItem(index)}>
                  <Trash />
                </div>
              </Col>
            </Row>
          )}
        </div>
        }
        <div className="new__invoice__items__btn">
          <Button
            type='button'
            coloring={'light'}
            rounding={'1.5rem'}
            width={'full'}
            textBold={'bold'}
            onClick={addItem}
          >Add New Item</Button>
        </div>
        <EditInvoiceModalActions
          onClose={onClose}
          onSave={onSaveChanges}
        />
      </form>
    </SlideModal>
  );
};