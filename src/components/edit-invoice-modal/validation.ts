import Joi from 'joi';
import { EditInvoiceProps } from '../../types/invoice';

export const editInvoiceValidation = Joi.object<EditInvoiceProps>({
  senderAddress: Joi.object({
    street: Joi.string().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
    postCode: Joi.string().required(),
  }).required(),
  clientAddress: Joi.object({
    street: Joi.string().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
    postCode: Joi.string().required(),
  }),
  clientName: Joi.string().required(),
  clientEmail: Joi.string().required(),
  description: Joi.string().required(),
  paymentDue: Joi.date().required(),
  paymentTerms: Joi.number(),
  items: Joi.array().items(
    Joi.object({
      name: Joi.string().required(),
      quantity: Joi.number().required(),
      price: Joi.number().required(),
      total: Joi.number(),
    })
  ),
  status: Joi.string(),
});