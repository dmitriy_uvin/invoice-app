import { InvoiceEntity } from '../../types';

export type InvoiceStatus = 'paid' | 'pending' | 'draft';

export interface InvoiceProps {
  data: InvoiceEntity;
}