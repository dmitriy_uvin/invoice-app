import { InvoiceProps } from './types';
import './styles.scss';
import { InvoiceStatus } from '../invoice-status';
import { ChevronRight } from '../icons';
import { Link } from 'react-router-dom';

export const Invoice = ({ data }: InvoiceProps) => {
  return (
    <div className="invoice">
      <div className="invoice__id">{data.id}</div>
      <div className="invoice__date">{data.createdAt}</div>
      <div className="invoice__author">{data.clientName}</div>
      <div className="invoice__amount">{data.total}</div>
      <div className="invoice__status__badge">
        <InvoiceStatus status={data.status} />
      </div>
      <div className="invoice__action">
        <Link to={`/invoice/${data.id}`}>
          <ChevronRight />
        </Link>
      </div>
    </div>
  );
};
