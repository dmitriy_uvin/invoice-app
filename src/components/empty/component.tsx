import emptyImg from '../../assets/icons/empty.png';
import './styles.scss';
import { Typography } from '../typography';

export const Empty = () => {
  return (
    <div className='empty__block'>
      <img src={emptyImg} alt=""/>
      <h3 className='empty__title'>
        <Typography type={'title'} text={'There is nothing here'} />
      </h3>
      <div className='empty__description'>
        <Typography type={'commonText'} text={
          <>Create an invoice by clicking the <b>'New Invoice'</b> button and get started</>
        } />
      </div>
    </div>
  );
};
