import './styles.scss';
import { ColProps } from './types';

export const Col = ({ children }: ColProps) => {
  return (
    <div className='app__col'>{children}</div>
  );
};