import { ComponentPropsWithRef, ReactNode } from 'react';

export interface ColProps extends ComponentPropsWithRef<'div'> {
  children: ReactNode;
}