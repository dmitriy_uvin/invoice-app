import moonIcon from '../../assets/icons/moon.svg';
import subIcon from '../../assets/icons/sun-icon.svg';
import { useState } from 'react';

export const ThemeSwitcher = () => {
  const [mode] = useState<string>(
    document.body.getAttribute('theme-mode') ?? 'light'
  );

  const onChange = () => {
    const currentThemeMode = document.body.getAttribute('theme-mode');
    if (!currentThemeMode) {
      document.body.setAttribute('theme-mode', 'dark');
      localStorage.setItem('theme-mode', 'dark');
      return;
    }
    document.body.removeAttribute('theme-mode');
    localStorage.setItem('theme-mode', 'light');
  };

  return (
    <div className="app__theme__switcher" onClick={onChange} style={{ cursor: 'pointer' }}>
      <img src={mode === 'light' ? moonIcon : subIcon} alt="theme-switcher-icon" />
    </div>
  );
};