import { Typography } from '../typography';
import { InvoiceAddress as InvoiceAddressProps } from '../../types/invoice';

export const InvoiceAddress = ({ address }: { address: InvoiceAddressProps }) => {
  return (
    <div className='invoice__address'>
      <Typography type={'commonTextDark'} text={address.street} />
      <Typography type={'commonTextDark'} text={address.city} />
      <Typography type={'commonTextDark'} text={address.postCode} />
      <Typography type={'commonTextDark'} text={address.country} />
    </div>
  );
};