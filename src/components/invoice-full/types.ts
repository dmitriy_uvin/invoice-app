import { InvoiceEntity } from '../../types';

export interface InvoiceFullProps {
  data: InvoiceEntity;
}

export interface InvoiceMobileInfoProps {
  data: InvoiceEntity;
}