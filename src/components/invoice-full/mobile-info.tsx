import { InvoiceMobileInfoProps } from './types';
import { Row } from '../row';
import { Col } from '../col';
import { Typography } from '../typography';
import { InvoiceAddress } from './invoice-address';

export const InvoiceMobileInfo = ({ data }: InvoiceMobileInfoProps) => {
  return (
    <div className='invoice__full__mobile'>
      <Row>
        <Col>
          <Typography type={'title'} text={`#${data.id}`} style={{ marginBottom: '.25rem' }} />
          <Typography type={'commonTextDark'} text={data.description} />
        </Col>
      </Row>
      <Row style={{ marginTop: '1.875rem' }}>
        <InvoiceAddress address={data.senderAddress} />
      </Row>
      <Row style={{ marginTop: '1.875rem' }}>
        <Col>
          <div>
            <Typography type={'commonTextDark'} text={'Invoice Date'} style={{ marginBottom: '.75rem' }} />
            <Typography type={'title'} text={data.createdAt} />
          </div>
          <div style={{ marginTop: '1.875rem' }}>
            <Typography type={'commonTextDark'} text={'Payment Due'} style={{ marginBottom: '.75rem' }} />
            <Typography type={'title'} text={data.paymentDue} />
          </div>
        </Col>
        <Col>
          <div style={{ marginBottom: '.5rem' }}>
            <Typography type={'commonTextDark'} text={'Bill To'} style={{ marginBottom: '.75rem' }} />
            <Typography type={'title'} text={data.createdAt} />
          </div>
          <InvoiceAddress address={data.clientAddress} />
        </Col>
      </Row>
      <Row style={{ marginTop: '2rem' }}>
        <div>
          <Typography type={'commonTextDark'} text={'Sent To'} style={{ marginBottom: '.75rem' }} />
          <Typography type={'title'} text={data.createdAt} />
        </div>
      </Row>
    </div>
  );
}