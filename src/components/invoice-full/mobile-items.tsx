import { Typography } from '../typography';
import { InvoiceItemsMobileProps } from '../invoice-mobile/types';
import { getItemsTotal } from '../../utils';

export const InvoiceItemsMobile = ({ items }: InvoiceItemsMobileProps) => {
  return (
    <div className='invoice__items__mobile'>
      <div className="invoice__items__mobile__data">
        {items.length > 0 && items.map((i, index) =>
          <div className="invoice__items__mobile__unit" key={index}>
            <div className="left">
              <div className="title">
                <Typography type={'title'} text={i.name} />
              </div>
              <div className="quantity">{i.quantity} x £ {i.price}</div>
            </div>
            <div className="right">
              <Typography type={'title'} text={`£ ${(i.quantity * i.price).toFixed(2)}`} />
            </div>
          </div>
        )}
      </div>
      <div className="invoice__items__mobile__total">
        <div className="title">Grand Total</div>
        <div className="amount">£ {getItemsTotal(items)}</div>
      </div>
    </div>
  );
}