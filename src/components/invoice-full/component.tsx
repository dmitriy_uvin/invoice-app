import { InvoiceFullProps } from './types';
import { Typography } from '../typography';
import './styles.scss';
import { useIsMobile } from '../../hooks';
import { InvoiceItemsMobile } from './mobile-items';
import { InvoiceMobileInfo } from './mobile-info';
import { InvoiceAddress } from './invoice-address';
import { getItemsTotal, priceFormatter } from '../../utils';

export const InvoiceFull = ({ data }: InvoiceFullProps) => {
  const isMobile = useIsMobile();

  return (
    <div className='invoice__full'>
      {!isMobile &&
          <>
              <div className="invoice__full__base">
                <div className="base__left">
                  <Typography type={'title'} text={`#${data.id}`} />
                  <Typography type={'commonText'} text={data.description} />
                </div>
                <div className="base__right">
                  <InvoiceAddress address={data.senderAddress} />
                </div>
              </div>
              <div className="invoice__full__main">
                  <div className="invoice__main__col">
                      <div className="invoice__main__item">
                          <div className="header">Invoice Date</div>
                          <div className="value">{data.createdAt}</div>
                      </div>
                      <div className="invoice__main__item">
                          <div className="header">Payment Due</div>
                          <div className="value">{data.paymentDue}</div>
                      </div>
                  </div>
                  <div className="invoice__main__col">
                      <div className="invoice__main__item">
                          <div className="header">Bill To</div>
                          <div className="value">{data.clientName}</div>
                          <InvoiceAddress address={data.clientAddress} />
                      </div>
                  </div>
                  <div className="invoice__main__col">
                      <div className="invoice__main__item">
                          <div className="header">Sent To</div>
                          <div className="value">{data.clientEmail}</div>
                      </div>
                  </div>
              </div>
          </>
      }
      {isMobile && <InvoiceMobileInfo data={data} />}
      {!isMobile && <div className="invoice__full__items">
        <div className="invoice__rows">
          <div className="invoice__rows__header">
            <div><Typography type={'commonText'} text={'Item Name'} /></div>
            <div><Typography type={'commonText'} text={'QTY.'} /></div>
            <div><Typography type={'commonText'} text={'Price'} /></div>
            <div><Typography type={'commonText'} text={'Total'} /></div>
          </div>
          {!!data.items.length && data.items.map((i, index) =>
            <div className="invoice__rows__data" key={index}>
              <div><Typography type={'title'} text={i.name} /></div>
              <div><Typography type={'commonText'} text={`${i.quantity}`} /></div>
              <div><Typography type={'commonText'} text={`£ ${i.price}`} /></div>
              <div><Typography type={'title'} text={`£ ${priceFormatter(i.price * i.quantity)}`} /></div>
            </div>
          )}
        </div>
        <div className="invoice__total">
          <div className="invoice__total__title">
            <Typography
              type={'commonText'}
              text={'Amount Due'}
              style={{ fontSize: 'calc(1rem - 3px)', color: '#ffffff' }}
            />
          </div>
          <div className="invoice__total__amount">
            <Typography
              type={'title'}
              text={`£ ${getItemsTotal(data.items)}`}
              style={{ fontSize: '1.5rem', color: '#ffffff' }}
            />
          </div>
        </div>
      </div>
      }
      {isMobile && <InvoiceItemsMobile items={data.items} />}
    </div>
  );
};