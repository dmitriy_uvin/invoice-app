import './styles.scss';
import logoSvg from '../../assets/icons/logo.svg';
import userAvatar from '../../assets/icons/user-avatar.png';
import { ThemeSwitcher } from '../theme-switcher';
import { Link } from 'react-router-dom';

export const Sidebar = () => {
  return (
    <div className="sidebar">
      <Link to={'/'} className="sidebar__logo">
        <img src={logoSvg} alt=""/>
      </Link>
      <div className="sidebar__footer">
        <div className="sidebar__theme__switcher">
          <ThemeSwitcher />
        </div>
        <div className="sidebar__user__info">
          <img src={userAvatar} alt=""/>
        </div>
      </div>
    </div>
  );
};