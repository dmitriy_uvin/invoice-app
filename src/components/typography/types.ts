import { ComponentPropsWithRef, ReactNode } from 'react';

export interface TypographyProps extends ComponentPropsWithRef<'div'>{
  type: 'title' | 'commonText' | 'commonTextDark';
  text: string | ReactNode;
}
