import { TypographyProps } from './types';
import './styles.scss';

export const Typography = ({ type, text, ...props }: TypographyProps) => {

  return (
    <div className={`typography__${type}`} {...props}>
      {text}
    </div>
  );
};