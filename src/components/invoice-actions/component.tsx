import './styles.scss';
import { InvoiceActionsProps } from './types';
import { InvoiceStatus } from '../invoice-status';
import { Button } from '../button';
import { useState } from 'react';
import { InvoiceEntity } from '../../types';
import { useToggle } from '../../hooks';
import { ConfirmDelete } from '../confirm-delete';
import { useNavigate } from 'react-router-dom';
import { Typography } from '../typography';
import { EditInvoiceModal } from '../edit-invoice-modal';
import { updateInvoice as updateInvoiceLC, deleteInvoice, updateStatus } from '../../local-storage';

export const InvoiceActions = ({ data, onUpdate }: InvoiceActionsProps) => {
  const [item, setItem] = useState<InvoiceEntity>(data);
  const statusBtn = !['paid'].includes(data.status);
  const [deleteModal, setDeleteModal] = useToggle();
  const navigation = useNavigate();
  const [editVisible, toggleEditVisible] = useToggle();

  const updateInvoice = (invoice: InvoiceEntity) => {
    updateInvoiceLC(invoice);
    setItem({ ...invoice });
    onUpdate();
  };

  const onDelete = () => {
    deleteInvoice(item.id);
    navigation('/');
  };

  const onSend = () => {
    setItem({
      ...item,
      status: 'pending',
    });
    updateStatus(item.id, 'pending');
  };

  const onPay = () => {
    setItem({
      ...item,
      status: 'paid',
    });
    updateStatus(item.id, 'paid');
  };

  return (
    <>
      <div className="invoice__actions">
        <div className="invoice__actions__status">
          <Typography type={'title'} text={'Status'} style={{ marginRight: '1.5rem' }}/>
          <InvoiceStatus status={item.status} />
        </div>
        <div className="invoice__actions__btns">
          <Button
            coloring={'light'}
            rounding={'24px'}
            textBold={'bold'}
            onClick={toggleEditVisible}
          >Edit</Button>
          <Button
            coloring={'error'}
            rounding={'24px'}
            textBold={'bold'}
            onClick={setDeleteModal}
          >Delete</Button>
          {statusBtn && item.status === 'draft' &&
              <Button
                  coloring={'primary'}
                  rounding={'24px'}
                  textBold={'bold'}
                  onClick={onSend}
              >
                  Send
              </Button>
          }
          {statusBtn && item.status === 'pending' &&
              <Button
                  coloring={'primary'}
                  rounding={'24px'}
                  textBold={'bold'}
                  onClick={onPay}
              >
                  Mark as Paid
              </Button>
          }
        </div>
      </div>
      {deleteModal && <ConfirmDelete
        onDelete={onDelete}
        onCancel={setDeleteModal}
        text={`Are you sure you want to delete invoice #${item.id}? This action cannot be undone.`}
      />}
      <EditInvoiceModal
        isVisible={editVisible}
        onClose={toggleEditVisible}
        onUpdate={updateInvoice}
        item={item}
      />
    </>
  );
};
