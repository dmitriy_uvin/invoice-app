import { InvoiceEntity } from '../../types';

export interface InvoiceActionsProps {
  data: InvoiceEntity;
  onUpdate: () => void;
}