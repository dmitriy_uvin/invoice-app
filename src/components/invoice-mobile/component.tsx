import { InvoiceProps } from './types';
import './styles.scss';
import { InvoiceStatus } from '../invoice-status';
import { useNavigate } from 'react-router-dom';
import { dateFormatter } from '../../utils';

export const InvoiceMobile = ({ data }: InvoiceProps) => {
  const navigate = useNavigate();

  const goToInvoice = () => {
    navigate(`/invoice/${data.id}`);
  }

  return (
    <div className="invoice__mobile" onClick={goToInvoice}>
      <div className="invoice__mobile__top">
        <div className="invoice__mobile__id">{data.id}</div>
        <div className="invoice__mobile__author">{data.clientName}</div>
      </div>
      <div className="invoice__mobile__bottom">
        <div className='left'>
          <div className="invoice__mobile__date">{dateFormatter(data.createdAt)}</div>
          <div className="invoice__mobile__amount">£ {data.total}</div>
        </div>
        <InvoiceStatus status={data.status} />
      </div>
    </div>
  );
};
