import { InvoiceEntity } from '../../types';
import { InvoiceItemEntity } from '../../types/invoice';

export type InvoiceStatus = 'paid' | 'pending' | 'draft';

export interface InvoiceProps {
  data: InvoiceEntity;
}

export interface InvoiceItemsMobileProps {
  items: InvoiceItemEntity[];
}