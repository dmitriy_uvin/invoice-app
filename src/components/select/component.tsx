import { AppSelectProps } from './types';
import { forwardRef } from 'react';

export const Select = forwardRef((
  {
    mode = 'default',
    error,
    label,
    values,
    ...props
  }: AppSelectProps,
  ref: any,
) => {
  const selectElem = (
    <>
      <select ref={ref} className={`app__input app__input--mode--${mode}`}>
        {values.length > 0 && values.map((v, index) =>
          <option value={v.value} key={index}>
          {v.name}
        </option>
        )}
      </select>
      {/*<input*/}
      {/*  ref={ref}*/}
      {/*  className={`app__input app__input--mode--${mode} ${error ? 'app__input--error': ''}`}*/}
      {/*  {...props}*/}
      {/*/>*/}
      {error && <span className="app__input__error__text">{error}</span>}
    </>
  );

  if (label) {
    return (
      <label className='app__input__label'>
        <p>{label}</p>
        {selectElem}
      </label>
    );
  }

  return selectElem;
});