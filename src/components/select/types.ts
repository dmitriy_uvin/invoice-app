import { ComponentPropsWithRef } from 'react';

export interface AppSelectProps extends ComponentPropsWithRef<'select'> {
  mode?: 'default';
  error?: string;
  label?: string;
  values: {
    value: string | number;
    name: string;
  }[]
}