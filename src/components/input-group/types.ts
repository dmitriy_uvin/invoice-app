import { ComponentPropsWithRef, ReactNode } from 'react';

export interface AppInputGroupProps extends ComponentPropsWithRef<'div'> {
  children: ReactNode;
  title?: string;
}