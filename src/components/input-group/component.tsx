import { AppInputGroupProps } from './types';
import './styles.scss';

export const InputGroup = ({ children, title, ...props }: AppInputGroupProps) => {
  return (
    <div className="app__input__group" {...props}>
      {title && <div className="app__input__group__title">{title}</div>}
      <div>
        {children}
      </div>
    </div>
  );
};