import { GoBackProps } from './types';
import { ChevronLeft } from '../icons';
import './styles.scss';
import { useNavigate } from 'react-router-dom';

export const GoBack = ({ destination }: GoBackProps) => {
  const navigator = useNavigate();
  const goBack = () => {
    if (destination) {
      navigator(destination);
      return;
    }
    navigator(-1);
  }

  return (
    <div className='go__back' onClick={goBack}>
      <ChevronLeft />
      <div className='go__back__title'>Go Back</div>
    </div>
  );
};