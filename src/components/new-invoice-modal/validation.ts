import Joi from 'joi';
import { NewInvoiceProps } from '../../types/invoice';

export const newInvoiceValidation = Joi.object<NewInvoiceProps>({
  senderAddress: Joi.object({
    street: Joi.string().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
    postCode: Joi.string().required(),
  }).required(),
  clientAddress: Joi.object({
    street: Joi.string().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
    postCode: Joi.string().required(),
  }),
  clientName: Joi.string().required(),
  clientEmail: Joi.string().required(),
  description: Joi.string().required(),
  paymentDue: Joi.date().required(),
  paymentTerms: Joi.number(),
  items: Joi.array().items(
    Joi.object({
      name: Joi.string().required(),
      quantity: Joi.number().required(),
      price: Joi.number().required(),
      // total: Joi.number().required(),
    })
  )
});