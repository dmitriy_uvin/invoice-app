import { InvoiceEntity } from '../../types';

export interface NewInvoiceModalProps {
  isVisible: boolean;
  onClose: () => void;
  onAdd: (invoice: InvoiceEntity) => void;
}

export interface NewInvoiceModalActionsProps {
  onClose: () => void;
  onSaveDraft: () => void;
  onSaveSend: () => void;
}