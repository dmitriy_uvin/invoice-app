import { Button } from '../button';
import { NewInvoiceModalActionsProps } from './types';

export const NewInvoiceModalActions = ({ onClose, onSaveDraft, onSaveSend }: NewInvoiceModalActionsProps) => {
  return (
    <div className="new__invoice__actions">
      <div className="left">
        <Button
          coloring={'light'}
          rounding={'1.5rem'}
          textBold={'bold'}
          type='button'
          onClick={onClose}
        >Discard</Button>
      </div>
      <div className="right">
        <Button
          coloring={'dark'}
          rounding={'1.5rem'}
          textBold={'bold'}
          type='button'
          onClick={onSaveDraft}
        >Save as Draft</Button>
        <Button
          coloring={'primary'}
          rounding={'1.5rem'}
          textBold={'bold'}
          type='button'
          onClick={onSaveSend}
        >Save & Send</Button>
      </div>
    </div>
  );
}