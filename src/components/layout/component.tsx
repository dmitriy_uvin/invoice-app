import { AppLayoutProps } from './types';
import { Sidebar } from '../sidebar';
import './styles.scss';
import { useIsTablet } from '../../hooks';

export const AppLayout = ({ children }: AppLayoutProps) => {
  const isTablet = useIsTablet();

  return (
    <div className="layout__wrapper">
      <Sidebar />
      <div className="layout__content">
        <div className="layout__container">
          {children}
        </div>
      </div>
    </div>
  );
};