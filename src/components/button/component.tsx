import { ButtonProps } from './types';
import './styles.scss';

export const Button = (
  {
    coloring = 'primary',
    rounding = '0px',
    sizing = 'default',
    textBold = 'normal',
    width = 'default',
    icon,
    children,
    ...props
  }: ButtonProps
) => {
  const btnClass = () => {
    const classes = ['app-btn'];
    classes.push(`btn--coloring--${coloring}`);
    classes.push(`btn--sizing--${sizing}`);
    classes.push(`btn--width--${width}`);
    classes.push(`btn--padding--default`);
    return classes.join(' ');
  };

  const getStyles = () => {
    let styles = {
      borderRadius: rounding,
      fontWeight: textBold,
      ...props.style,
    };
    if (icon) {
      styles.padding = '.5rem 1rem .5rem .5rem';
    }
    return styles;
  }

  return (
    <button
      {...props}
      className={btnClass()}
      style={getStyles()}
    >
      {!!icon && <div style={{ marginRight: '1rem' }}>
        {icon}
      </div>}
      {children}
    </button>
  );
};