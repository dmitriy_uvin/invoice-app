import { ComponentPropsWithRef, ReactNode } from 'react';

type ButtonColoring = 'primary' | 'error' | 'light' | 'dark';
type ButtonSizing = 'default';
type ButtonWidth = 'default' | 'full';
type ButtonTextBold = 'normal' | 'bold' | string;
type ButtonRounding = 'default' | string;

export interface ButtonProps extends ComponentPropsWithRef<'button'> {
  coloring?: ButtonColoring;
  rounding?: ButtonRounding;
  sizing?: ButtonSizing;
  textBold?: ButtonTextBold;
  icon?: ReactNode;
  width?: ButtonWidth;
}