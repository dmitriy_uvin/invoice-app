import { ComponentPropsWithRef } from 'react';

export interface AppInputProps extends ComponentPropsWithRef<'input'> {
  mode?: 'default';
  error?: string;
  label?: string;
  displayError?: boolean;
}