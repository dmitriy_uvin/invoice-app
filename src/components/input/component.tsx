import { AppInputProps } from './types';
import './styles.scss';
import { forwardRef } from 'react';

export const Input = forwardRef((
  {
    mode = 'default',
    displayError = true,
    error,
    label,
    ...props
  }: AppInputProps,
  ref: any,
) => {
  const inputElem = (
    <>
      <input
        ref={ref}
        className={`app__input app__input--mode--${mode} ${error ? 'app__input--error': ''}`}
        {...props}
      />
      {error && displayError && <span className="app__input__error__text">{error}</span>}
    </>
  );

  if (label) {
    return (
      <label className='app__input__label'>
        <p>{label}</p>
        {inputElem}
      </label>
    );
  }

  return inputElem;
});