import { ReactNode } from 'react';

export interface SideModalProps {
  isVisible: boolean;
  children: ReactNode;
}