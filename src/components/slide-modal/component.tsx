import './styles.scss';
import { SideModalProps } from './types';

export const SlideModal = ({ isVisible, children }: SideModalProps) => {
  return (
    <div
      className={`slide__modal ${isVisible ? 'slide__modal--open slide__modal__bg' : ''}`}
    >
      <div className={`slide__modal__wrapper ${isVisible ? 'open' : ''}`}>
        {children}
      </div>
    </div>
  );
};