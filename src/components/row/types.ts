import { ComponentPropsWithRef, ReactNode } from 'react';

export interface RowProps extends ComponentPropsWithRef<'div'> {
  children: ReactNode;
  cols?: number;
}