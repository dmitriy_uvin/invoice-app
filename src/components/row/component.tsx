import './styles.scss';
import { RowProps } from './types';

export const Row = ({ children, cols = 1, className, ...props }: RowProps) => {
  return (
    <div
      className={`${className} app__row app__row__${cols}`}
      {...props}
    >{children}</div>
  );
};