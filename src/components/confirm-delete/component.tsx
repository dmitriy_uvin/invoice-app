import './styles.scss';
import { ConfirmDeleteModalProps } from './types';
import { Typography } from '../typography';
import { Button } from '../button';

export const ConfirmDelete = ({ onDelete, onCancel, text }: ConfirmDeleteModalProps) => {
  return (
    <div className="confirm__delete__bg">
      <div className="confirm__delete__modal">
        <Typography
          type={'title'}
          text={'Confirm Deletion'}
          style={{ fontSize: '1.5rem', marginBottom: '1rem' }}
        />
        <Typography type={'commonText'} text={text} style={{ lineHeight: '1.5rem' }} />
        <div className="confirm__delete__actions">
          <Button
            coloring={'light'}
            rounding={'24px'}
            textBold={'bold'}
            onClick={onCancel}
          >Cancel</Button>
          <Button
            coloring={'error'}
            rounding={'24px'}
            textBold={'bold'}
            onClick={onDelete}
          >Delete</Button>
        </div>
      </div>
    </div>
  );
};