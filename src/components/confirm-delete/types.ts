export interface ConfirmDeleteModalProps {
  onCancel: () => void;
  onDelete: () => void;
  text: string;
}
