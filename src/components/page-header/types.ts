import { ReactNode } from 'react';

export interface PageHeaderProps {
  left?: ReactNode;
  right?: ReactNode;
}