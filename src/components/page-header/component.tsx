import './styles.scss';
import { PageHeaderProps } from './types';


export const PageHeader = (props: PageHeaderProps) => {
  return (
    <div className='page__header'>
      <div className="page__header__left">
        {props.left}
      </div>
      <div className="page__header__right">
        {props.right}
      </div>
    </div>
  );
};