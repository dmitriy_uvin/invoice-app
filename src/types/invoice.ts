export type InvoiceStatus = 'paid' | 'pending' | 'draft';

export interface InvoiceItemEntity {
  name: string;
  quantity: number;
  price: number;
  total: number;
}

export interface InvoiceAddress {
  street: string;
  city: string;
  postCode: string;
  country: string;
}

export interface InvoiceEntity {
  id: string;
  createdAt: string;
  paymentDue: string;
  description: string;
  paymentTerms: number;
  clientName: string;
  clientEmail: string;
  status: InvoiceStatus;
  senderAddress: InvoiceAddress,
  clientAddress: InvoiceAddress,
  items: InvoiceItemEntity[],
  total: number;
}

export interface NewInvoiceProps {
  paymentDue: string;
  description: string;
  paymentTerms: number;
  clientName: string;
  clientEmail: string;
  status: InvoiceStatus;
  clientAddress: InvoiceAddress;
  senderAddress: InvoiceAddress;
  items: InvoiceItemEntity[];
}

export interface EditInvoiceProps {
  id: string;
  createdAt: string;
  paymentDue: string;
  description: string;
  paymentTerms: number;
  clientName: string;
  clientEmail: string;
  status: InvoiceStatus;
  clientAddress: InvoiceAddress;
  senderAddress: InvoiceAddress;
  items: InvoiceItemEntity[];
}