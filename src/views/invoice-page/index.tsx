import { useParams } from 'react-router-dom';
import { AppLayout, Empty, InvoiceActions, PageHeader } from '../../components';
import { GoBack } from '../../components';
import { InvoiceFull } from '../../components';
import { getById } from '../../local-storage';
import { InvoiceEntity } from '../../types';
import { useState } from 'react';

export const IndexPage = () => {
  const { id } = useParams();
  const [item, setItem] = useState<InvoiceEntity>(getById(id as string));

  const onUpdate = () => {
    setItem({
      ...getById(id as string)
    });
  };

  if (!item) {
    return (
      <AppLayout>
        <PageHeader
          left={<GoBack />}
        />
        <div style={{ marginTop: '6.3rem' }}>
          <Empty />
        </div>
      </AppLayout>
    );
  }

  return (
    <AppLayout>
      <PageHeader
        left={<GoBack />}
      />

      <InvoiceActions data={item} onUpdate={onUpdate} />

      <InvoiceFull data={item} />
    </AppLayout>
  );
};
