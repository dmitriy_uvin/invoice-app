import { AppLayout, Invoice, PageHeader } from '../../components';
import { Empty } from '../../components';
import { Button } from '../../components';
import './styles.scss';
import { PlusIcon } from '../../components';
import { useInvoices, useIsMobile, useToggle } from '../../hooks';
import { NewInvoiceModal } from '../../components';
import { InvoiceEntity } from '../../types';
import { InvoiceMobile } from '../../components/invoice-mobile';
import { saveNewInvoicesArray } from '../../local-storage';

export const MainPage = () => {
  const [invoiceModal, setInvoiceModal] = useToggle();
  const [items, setItems] = useInvoices();
  const isMobile = useIsMobile();

  const onAddInvoice = (invoice: InvoiceEntity) => {
    const newItems = [...items];
    newItems.push(invoice);
    setItems([...newItems]);
    saveNewInvoicesArray(newItems);
  };


  const getInvoiceComponent = (invoice: InvoiceEntity) => {
    if (isMobile) return <InvoiceMobile data={invoice} key={invoice.id} />;
    return <Invoice data={invoice} key={invoice.id} />
  }

  return (
    <AppLayout>
      <PageHeader
        left={
        <>
          <div className="invoices__header__title">Invoices</div>
          <div className="invoices__header__desc">There are 7 total invoices</div>
        </>
        }
        right={
        <>
          <div className="invoices__header__right">
            <p className='invoices__header__filter'>
              Filter{isMobile ? '' : ' by status'}
            </p>
            <Button
              rounding={'24px'}
              textBold={'bold'}
              icon={<PlusIcon />}
              onClick={setInvoiceModal}
              style={{ padding: '.5rem !important' }}
            >
              New{isMobile ? '' : ' Invoice'}
            </Button>
          </div>
        </>
        }
      />
      <div className='invoices__list'>
        {!items.length && <Empty />}
        {!!items.length && items.map((i) => getInvoiceComponent(i))}
      </div>
      <NewInvoiceModal
        isVisible={invoiceModal}
        onClose={() => setInvoiceModal()}
        onAdd={onAddInvoice}
      />
    </AppLayout>
  );
};